#Sentence Generation for Entity Description with Content-plan Attention
An open-source framework for generating entity description from structured data.

# Content
1. Entity Description Generation.ipynb.ipynb
2. data/

#Requirement
1. Python (>=2.7)
2. Numpy (>=1.13.3)
3. TensorFlow (>=1.4.1)
4. CUDA (>=9.0)
5. Matplotlib (>=2.0.0)
6. scikit-learn (>=0.18)

#References
Bayu Distiawan Trisedya, Jianzhong Qi, Rui Zhang. [**Sentence Generation for Entity Description with Content-plan Attention.**](https://bitbucket.org/bayudt/ent_desc/src/master/paper/Entity_Description.pdf) AAAI 2019.